import React, { Component } from 'react';
import { List, AutoSizer } from "react-virtualized";
import data from "./data.json";

const rowHeight = 70;
const overscanRowCount = 3;

const style = {
   width: '200px',
   padding: '10px',
   marginBottom: '10px'
};

class App extends Component {
    state = {
      list: data
    };

  renderRow = ({ index, key, style }) => {
    const { list } = this.state;

    return (
        <ul key={key} style={style}>
            <li>Name: {list[index].name}</li>
            <li>Age: {list[index].age}</li>
            <li>Eye color: {list[index].eyeColor}</li>
        </ul>
    );
  };

  onChangeSearch = (e) => {
    const  { value } = e.target;

    const listFilter = data.filter(row => row.name.toLowerCase().includes(value.toLowerCase()));

    this.setState({
      list: listFilter
    })
  };

  render() {
    const { list } = this.state;
    const listLen = list.length;

    return (
        <div className="App">
          <div id='search'>
            <input type='text'
                   placeholder='Search by name'
                   onChange={this.onChangeSearch}
                   style={style}
            />
          </div>
          <div style={{height: 'calc(100vh - 70px)'}}>
            {
              !listLen && <h1>Не найдено</h1>
            }
            <AutoSizer>
              {
                ({ width, height }) => {
                  return <List
                      width={width}
                      height={height}
                      rowHeight={rowHeight}
                      rowRenderer={this.renderRow}
                      rowCount={listLen}
                      overscanRowCount={overscanRowCount}
                  />
                }
              }
            </AutoSizer>
          </div>
        </div>
    );
  }
}


export default App;
